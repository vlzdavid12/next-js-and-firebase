import React,  {useEffect, useState, useContext} from 'react';
import {FirebaseContext} from "../firebase";
const useProducts = order  =>{

    const [products, setProducts] = useState([]);
    const {firebase} = useContext(FirebaseContext);


    useEffect(()=>{
        const getProduct = () => {
            firebase.db.collection('products').orderBy(order, 'desc').onSnapshot(createSnapshop)
        }
        getProduct()
    },[])

    function createSnapshop(snapshot){
        const products = snapshot.docs.map(doc => {
            return{
                id: doc.id,
                ...doc.data()
            }
        })
        setProducts(products);
    }

    return {products}

}

export default useProducts;
