import React, {useEffect, useState} from 'react';
import firebase from '../firebase';

function useAuthentication(){
    const [userAuthenticate, setUserAuthenticate] = useState(null);

    useEffect(()=>{
        const unsuscribe = firebase.auth.onAuthStateChanged(user=>{
            if(user){
                    setUserAuthenticate(user);
            }else{
                    setUserAuthenticate(false);
            }
        });

        return()=>unsuscribe();

    },[]);

    return userAuthenticate;

}
export default  useAuthentication;
