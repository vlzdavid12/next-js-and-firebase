import React, {useState, useEffect} from 'react';

const useValidation = (stateInitial, validate, fn) => {
    const [state, setState] = useState(stateInitial);
    const [errors, setErrors] = useState({});
    const [submit, setSubmit] = useState(false);

    useEffect(() => {
        if (submit) {
            const notErrors = Object.keys(errors).length === 0;
            if (notErrors) {
                fn();
            }
            setSubmit(false);
        }
    }, [errors])


    //Function execute on write
    const handleChange = e => {
        setState({
            ...state,
            [e.target.name]: e.target.value
        })
    }

    //Function Execute Submit

    const handleSubmit = e => {
        e.preventDefault();
        const errorsValidation = validate(state);
        setErrors(errorsValidation);
        setSubmit(true);
    }

    //Function event Blur
    const handleBlur = () =>{
        const errorsValidation = validate(state);
        setErrors(errorsValidation);
    }

    return {
        state,
        errors,
        submit,
        handleSubmit,
        handleChange,
        handleBlur
    };
}

export default useValidation;
