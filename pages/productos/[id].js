import React, {useEffect, useContext, useState} from 'react';
import {useRouter} from "next/router";
import Layout from "../../components/layouts/Layout";
import {FirebaseContext} from '../../firebase/';
import PageNoFount from "../../pages/404";
import {Campo, InputSubmit} from "../../components/ui/formulario";
import Buton from "../../components/ui/Buttom";

//Date FNS
import formatDistanceToNow from 'date-fns/formatDistanceToNow'
import {es} from 'date-fns/locale'

//Import Styled
import styled from "@emotion/styled";
import {css} from "@emotion/react";


const Image =  styled.img`
      margin: 5px auto;
      display:block
`;

const ContainerProduct =  styled.div`
    display: grid;
    grid-template-columns: 2fr 1fr;
    column-gap: 2rem;
    max-width: 890px;
    margin: auto;
`

const Author = styled.span`
    font-size: 12px;
    color: var(--naranja);
    font-family: "Roboto Thin", sans-serif;
    margin-top: 3px;
    margin-left: 3px;
`

const Product = () => {
    //Router Get ID
    const router = useRouter();
    const {query: {id}} = router;
    const [product, setProduct] =  useState([]);
    const [errorProduct, setErrorPorduct] = useState(false);
    const [articleComments , setArticleComments] = useState({});

    //Consult General
    const { user ,firebase} = useContext(FirebaseContext);

    const [consulDB, setConsulDB] = useState(true);


    useEffect(()=>{
        if(id && consulDB){
            const getProduct = async() =>{
                const productQuery = await firebase.db.collection('products').doc(id);
                const product = await productQuery.get();
                if(product.exists){
                    setProduct(product.data());
                    setConsulDB(false);
                }else{
                    setErrorPorduct(true);
                }
            }
            getProduct();
        }
    }, [id])

    if(errorProduct) return <PageNoFount />

    if(Object.keys(product).length === 0 ) return <div>Cargando ...</div>;

    //Data Firebase Consult
    const {urlimagen, nombre, create, url, votes, comments, business, desc, creado, inVotes} = product


    //Function Votes

    const handleVote= async () => {
        if(!user) return router.push('/login');

        //Get New Vote
        const newTotal = votes + 1;

        if(inVotes.includes(user.uid)) return;

        const newVotes = [ ...inVotes, user.uid];

        //Update Base de Datos
        await firebase.db.collection('products').doc(id).update({votes: newTotal, inVotes: newVotes});

        //Update State
        setProduct({
            ...product,
            votes: newTotal,
        })

        //Start UseEffect
        setConsulDB(false);
    }

    //Function Comments Create

    const commentsChange = (e) => {
       setArticleComments({
           ...articleComments,
           [e.target.name]: e.target.value
       })
    }


    const addArticleComments = async (e) => {
        e.preventDefault();

        if(!user) return router.push('/login');

        //Object Comments
        articleComments.userid = user.uid
        articleComments.userName = user.displayName

        //Get New Comments
        const newComments =  [...comments, articleComments];

        //Update Base de Datos
        await firebase.db.collection('products').doc(id).update({ comments: newComments})

        //Update State
        setProduct({
            ...product,
            comments: newComments
        })

        //Start UseEffect
        setConsulDB(true);

    }


    const creator = id =>{
        if(creado.id = id){
            return true;
        }
    }



    //Delete Product
    const deleteProduct = async () =>{
        if(!user) return router.push('/login');
        if(creado.id != user.uid) return router.push('/');

        try{
            await firebase.db.collection('products').doc(id).delete();
            return router.push('/');
        }catch (error){
            console.log(error)
        }
    }

    return (<Layout>
        <div>
            <ContainerProduct>
            <div>
                <Image src={urlimagen} alt={nombre} />
                <div className="contenedor">
                    <h1 css={css`margin: 0`}>{nombre}</h1>
                    <p css={css` font-size: 12px; margin: 0`}>Publicado: {formatDistanceToNow(new Date(create), {locale: es})} por {creado?.name} de {business}</p>
                    <p>{desc}</p>
                </div>
                {user &&(<><h3>Agregar Comentario</h3>
                <form onSubmit={addArticleComments}>
                    <Campo>
                       <textarea
                           name="msg"
                           placeholder="Ingrese el comentario"
                           onChange={commentsChange}
                       ></textarea>
                    </Campo>
                    <InputSubmit
                        type="submit"
                        value="Agregar Comentario"
                    />
                </form></>)}
                <h3 css={css` margin-top: 10px`}>Comentarios</h3>

                {comments.length === 0 ? <p> !Aun no hay comentarios¡ </p> : <ul>
                    {comments.map(comment =>
                        (<li key={comment.userid}>
                            <p css={css`font-size: 14px`}>{comment.msg}</p>
                            <h5 css={css`display: flex; color: #575757;`}><i className="icon-author"></i>{comment.userName} | {creator(comment.userid) && <Author> Author Original </Author> }</h5>

                        </li>)
                    )}
                </ul>}
            </div>
            <aside>
                <Buton css={css` margin-top: 5px;`} target="_blank" href={url}  bgColor={true} >Ver Mas</Buton>
                {user &&(<Buton css={css` margin-top: 5px;`}  bgColor={false} onClick={handleVote} >Votar</Buton>)}
                <p css={css`text-align: center`}>Votos: {votes}</p>
            </aside>
                {(creado?.id === user.uid)? <Buton onClick={deleteProduct} >Eliminar Producto</Buton> : null}
            </ContainerProduct>

        </div>
    </Layout>)
}
export default Product;
