import React from 'react';
import {css} from '@emotion/react'
import Layout from "../components/layouts/Layout";
const PageNoFount = ( ) =>{
    return (<Layout>
       <h2 css={css`
        text-align: center;
        margin: 70px auto;
       `}>No Se Puede Mostrar</h2>
    </Layout>)
}
export default  PageNoFount;
