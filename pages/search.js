import React, {useEffect, useState} from 'react';
import Layout from '../components/layouts/Layout';
import DetailsProduct from "../components/layouts/DetailsProduct";
import useProducts from "../hooks/useProducts";
import {useRouter} from "next/router";
import {css} from '@emotion/react';

const NewProduct = () => {

    const router = useRouter();
    const {query: {q}} = router;

    const {products} = useProducts('create');
    const [results, setResult] = useState([]);

    useEffect(() => {
        if(q === undefined)  return router.push('/')
        const search = q.toLowerCase();
        const filter = products.filter(product => {
        return(product.nombre.toLowerCase().includes(search)
            || product.nombre.toLowerCase().includes(search));
        })
        setResult(filter);
    }, [q, products])

    return (
        <Layout>
            {results.length > 0?
                (<div className="list-products">
                    <ul className="container">
                        {results.map(product => <DetailsProduct key={product.id} result={product} /> )}
                    </ul>
                </div>) : <h3 css={css`text-align: center; margin-top: 20%` }> No hay resultados en la consulta</h3> }

        </Layout>
    )
}
export default NewProduct;
