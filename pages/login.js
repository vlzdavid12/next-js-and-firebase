import React, {useState} from 'react';
import firebase from '../firebase'
import { css }  from '@emotion/react';
import {Formulario, Campo, InputSubmit, Error} from "../components/ui/formulario";

//Import Router
import Router from 'next/router';

import Layout from '../components/layouts/Layout';
import useValidations from "../hooks/useValidations";
import validateIniciarSession from "../validations/validateIniciarSession";


const STATE_INICIAL = {
    email: '',
    password: ''
}


const Login = () =>{

    const [error, setError] = useState(false);

    const {
        state,
        errors,
        submit,
        handleSubmit,
        handleChange,
        handleBlur
    } =  useValidations(STATE_INICIAL, validateIniciarSession, startAccount);


    const {email, password} =  state;

    async function startAccount(){
     try{
         const user = await  firebase.login(email, password);
         console.log(user);
         Router.push("/");
     }catch(error){
         console.log('Hubo un error en el inicio de session', error)
         setError(error.message)
     }
    }

    return(<Layout>
        <>
            <h1 css={
                css`
text-align: center;
margin-top: 5rem;                
`
            }>Iniciar Sesión</h1>
            <Formulario
                onSubmit={handleSubmit}
                noValidate >

                <Campo>
                    <label htmlFor="email" > Correo Electrónico </label>
                    <input
                        type="email"
                        id="email"
                        placeholder="Correo Electrónico"
                        name="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Campo>
                {errors.email && <Error>{errors.email}</Error>}
                <Campo>
                    <label htmlFor="email" > Contraseña </label>
                    <input
                        type="password"
                        id="password"
                        placeholder="Ingresa contraseña"
                        name="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Campo>
                {errors.password && <Error>{errors.password}</Error>}
                <InputSubmit type="submit" value="Iniciar Sesión" />
                {error && <Error>{error}</Error>}
            </Formulario>

        </>
    </Layout>)
}
export default Login;
