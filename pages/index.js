import React, {useEffect, useState, useContext} from 'react'
import Layout from "../components/layouts/Layout";
import DetailsProduct from "../components/layouts/DetailsProduct";
import useProducts from "../hooks/useProducts";


const Home = () => {

    const {products} = useProducts('create');
  return (
    <div>
        <Layout>
            <div className="list-products">
                <ul className="container">
                    {products.map(product => <DetailsProduct key={product.id} result={product} /> )}
                </ul>
            </div>
        </Layout>
    </div>
  )
}
export default Home
