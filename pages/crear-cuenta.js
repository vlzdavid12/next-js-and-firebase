import React, {useState} from 'react';
import firebase from '../firebase'
import { css }  from '@emotion/react';
import {Formulario, Campo, InputSubmit, Error} from "../components/ui/formulario";

//Import Router
import Router from 'next/router';

import Layout from '../components/layouts/Layout';
import useValidations from "../hooks/useValidations";
import validateCuenta from "../validations/validateCuenta";

const STATE_INICIAL = {
    nombre: '',
    email: '',
    password: ''
}


const CreateCuenta = () =>{

    const [error, setError] = useState(false);

    const {
        state,
        errors,
        submit,
        handleSubmit,
        handleChange,
        handleBlur
    } =  useValidations(STATE_INICIAL, validateCuenta, createAccount);


    const {nombre, email, password} =  state;

    async function createAccount(){
        try{
            await firebase.register(nombre, email, password);
            Router.push('/');
        }catch (error){
            console.log('Hubo un error al crear dicho usuario', error.message);
            setError(error.message)
        }
    }

    return(
        <Layout>
            <>
            <h1 css={
                css`
text-align: center;
margin-top: 5rem;                
`
            }>Crear Cuenta</h1>
                <Formulario
                    onSubmit={handleSubmit}
                    noValidate >
                    <Campo>
                        <label htmlFor="nombre" > Nombre </label>
                        <input
                            type="text"
                            id="nombre"
                            placeholder="Nombre Completo"
                            name="nombre"
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                    </Campo>
                    {errors.nombre && <Error>{errors.nombre}</Error>}
                    <Campo>
                        <label htmlFor="email" > Correo Electrónico </label>
                        <input
                            type="email"
                            id="email"
                            placeholder="Correo Electrónico"
                            name="email"
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                    </Campo>
                    {errors.email && <Error>{errors.email}</Error>}
                    <Campo>
                        <label htmlFor="email" > Contraseña </label>
                        <input
                            type="password"
                            id="password"
                            placeholder="Ingresa contraseña"
                            name="password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                    </Campo>
                    {errors.password && <Error>{errors.password}</Error>}
                 <InputSubmit type="submit" value="Registrarse" />
                    {error && <Error>{error}</Error>}
                </Formulario>

            </>
        </Layout>
    )
}
export default CreateCuenta;

