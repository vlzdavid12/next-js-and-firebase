import React, {useState, useContext} from 'react';
import FileUploader from "react-firebase-file-uploader";
import {Formulario, Campo, InputSubmit, Error, Success} from "../components/ui/formulario";
import {css} from '@emotion/react';

//Import Router
import Router, {useRouter} from 'next/router';
import {FirebaseContext} from '../firebase';

import Layout from '../components/layouts/Layout';
import useValidations from "../hooks/useValidations";
import validateCreateProduct from "../validations/validateCreateProduct";
import PageNoFount from "./404";

const STATE_INICIAL = {
    nombre: '',
    business: '',
    imagen: '',
    url: '',
    desc: '',
}


const NewProduct = () => {

    //State image Url
    const [nameImage, setNameImage] = useState('');
    const [upload, setUpload] = useState(false);
    const [progress, setProgress] = useState(0);
    const [urlimagen, setUrlImagen] = useState('');

    const [error, setError] = useState(false);

    const {
        state,
        errors,
        submit,
        handleSubmit,
        handleChange,
        handleBlur
    } = useValidations(STATE_INICIAL, validateCreateProduct, saveProduct);


    const {nombre, business, imagen, url, desc} = state;

    //Hook Redirect
    const router = useRouter();

    //Context Operations Crud Firebase
    const {user, firebase} = useContext(FirebaseContext);

    async function saveProduct() {
        //Verify User
        if (!user) {
            return router.push('/login');
        }

        //Create Object new product
        const product = {
            nombre,
            business,
            url,
            desc,
            urlimagen: urlimagen,
            votes: 0,
            comments: [],
            create: Date.now(),
            creado: {
                id: user.uid,
                name: user.displayName
            },
            inVotes:[]
        }

        //Insert Datos
        firebase.db.collection('products').add(product);
        return router.push('/');

    }

    const handleUploadStart = () => {
        setUpload(true);
        setProgress(0);
    }

    const handleProgress = progress => setProgress(progress);

    const handleUploadError = error => {
        setUpload(false)
        console.error(error);
    }

    const handleUploadSuccess = async filename => {
        setProgress(100)
        setUpload(false)
        setNameImage(filename)
        await firebase
            .storage
            .ref("products")
            .child(filename)
            .getDownloadURL()
            .then(url => {
                setUrlImagen(url)
            })
    }

    if(!user) return <PageNoFount/>

    return (
        <Layout>
            <>
                <h1 css={
                    css`
                      text-align: center;
                      margin-top: 5rem;
                    `
                }>Nuevo Producto</h1>
                <Formulario
                    onSubmit={handleSubmit}
                    noValidate>
                    <fieldset>
                        <legend>Información General</legend>

                        <Campo>
                            <label htmlFor="nombre"> Nombre </label>
                            <input
                                type="text"
                                id="nombre"
                                placeholder="Nombre Completo"
                                name="nombre"
                                value={nombre}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </Campo>
                        {errors.nombre && <Error>{errors.nombre}</Error>}
                        <Campo>
                            <label htmlFor="business"> Empresa </label>
                            <input
                                type="text"
                                id="business"
                                placeholder="Nombre Empresa"
                                name="business"
                                value={business}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </Campo>
                        {errors.business && <Error>{errors.business}</Error>}
                        <Campo>
                            <label htmlFor="imagen"> Imagen Producto </label>
                            <FileUploader
                                accept="image/*"
                                id="imagen"
                                name="imagen"
                                randomizeFilename
                                storageRef={firebase.storage.ref("products")}
                                onUploadStart={handleUploadStart}
                                onUploadError={handleUploadError}
                                onUploadSuccess={handleUploadSuccess}
                                onProgress={handleProgress}
                            />
                        </Campo>
                        {progress >= 100 ? <Success>Archivo subido correctamente</Success> : null}

                        <Campo>
                            <label htmlFor="url"> Enlace o Url </label>
                            <input
                                type="url"
                                id="url"
                                placeholder="Ingresa la url"
                                name="url"
                                value={url}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </Campo>
                        {errors.url && <Error>{errors.url}</Error>}
                    </fieldset>
                    <fieldset>
                        <legend>Sobre su producto</legend>

                        <Campo>
                            <label htmlfor="desc">Descriptión</label>
                            <textarea
                                type="desc"
                                id="desc"
                                placeholder="Ingresa la descripción del producto"
                                name="desc"
                                value={desc}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </Campo>
                        {errors.desc && <Error>{errors.desc}</Error>}
                    </fieldset>

                    <InputSubmit type="submit" value="Crear Producto"/>
                    {error && <Error>{error}</Error>}
                </Formulario>
            </>
        </Layout>
    )
}
export default NewProduct;
