export default function validateCreateProduct(valores){

    let errors = {}

    if(!valores.nombre){
        errors.nombre = "El nombre del producto";
    }

    if(!valores.business){
        errors.business = "Ingresa la empresa de dicho producto";
    }

    if(!valores.url){
        errors.url = "Ingresa la url del producto";
    }else if(!/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/.test(valores.url)){
        errors.url = "Ingresa la url correctamente";
    }

    if(!valores.desc){
        errors.desc = "Ingresa la descriptión del producto";
    }

    return errors;
}
