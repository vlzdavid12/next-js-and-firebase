export default function validateIniciarSession(valores){
    let errors = {}

    if(!valores.email){
        errors.email = "Error en el correo electronico";
    }else if( !/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i.test(valores.email)){
        errors.email= "Email no es valido";
    }


    if(!valores.password){
        errors.password = "La contraseña es obligatoria";
    }else if(valores.password.length < 6){
        errors.password = "Minimo de 6 caracteres";
    }

    return errors;
}
