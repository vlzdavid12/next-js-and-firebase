import React, {useState} from 'react';
import styled from '@emotion/styled';
import {css} from '@emotion/react';
import Router from 'next/router';

const InputText = styled.input`
    border: 1px solid var(--gris3);
    padding: 1rem;
    min-width: 290px;
    max-height: 50px;
    border-radius: 4px;
`;

const InputSubmit = styled.button`
    height: 3rem;
    width: 3rem;
    display: block;
    background-size: 2.5rem;
    background-image: url('/static/img/search.png');
    background-repeat: no-repeat;
    position: absolute;
    top: 4px;
    right: 1px;
    background-color: transparent;
    border: 0px;
    text-indent: -9999px;
  &:hover{
    cursor: pointer;
  }
`;


  const Search = ( ) =>{
    const [search, setSearch] = useState('');

    const searchProduct = (e) =>{
        e.preventDefault();

        if(search.trim() === '') return;

        //Redirect of User Search
        Router.push({
            pathname: '/search/',
            query:{
                "q": search
            }
        })
    }

    return(
      <form
        css={
            css`
            position: relative;           
            `}
        onSubmit={searchProduct}
      >
          <InputText type="text"
                     onChange={(e)=>setSearch(e.target.value)}
                     placeholder="Buscar productos"
          />
          <InputSubmit type="submit">Buscar</InputSubmit>
      </form>
    )
}

export default  Search;
