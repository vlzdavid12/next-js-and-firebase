import styled from '@emotion/styled';

export const Formulario = styled.form`
  max-width: 600px;
  width: 65%;
  margin: 5rem auto 0 auto;
  
  fieldset{
    margin: 2rem 0;
    border: 1px solid #e1e1e1;
  }
  
  
`;

export const Campo = styled.div`
  margin-bottom: 2rem;
  display: flex;
  align-items: center;
  
  label {
    flex: 0 0 150px;
    font-size: 1.8rem;
    font-size: 14px;
  }

  input {
    flex: 1;
    padding: 1rem;
    border: 1px solid #b3b3b3;
    border-radius: 4px;
  }
  
  textarea{
    width: 100%;
    height: 200px;
    font-family: 'Roboto Thin', sans-serif;
    font-size: 1.1rem;
    padding: 1rem;
  }
  
`;


export const InputSubmit = styled.input`
  background-color: var(--naranja);
  width: 100%;
  padding: 1rem;
  text-align: center;
  color: #fff;
  font-size: 1.1rem;
  text-transform: uppercase;
  border: 1px solid var(--naranja);
  border-radius: 4px;

  &:hover {
    cursor: pointer;
  }
`

export const Error = styled.div`
    background-color: #ea0f0f87;
    padding: 1rem;
    font-family: 'Roboto Thin', sans-serif;
    border-radius: 2px;
    margin-bottom: 10px;
    margin-top:10px;
    color:#fff;
    font-size: 1.4rem;
`

export const Success = styled.div`
  background-color: #178f3bc4;
  padding: 1rem;
  font-family: 'Roboto Thin', sans-serif;
  border-radius: 2px;
  margin-bottom: 10px;
  margin-top: 10px;
  color: #fff;
  font-size: 1.4rem;

`

