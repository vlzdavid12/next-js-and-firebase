import styled from '@emotion/styled';
const Buton = styled.a`
  font-weight: 700;
  text-transform: uppercase;
  text-decoration: none;
  border: 1px solid ${props => props.bgColor ?  '#DA552F' : '#EfEfEf'};
  border-radius: 2px;
  padding: .8rem 2rem;
  display: block;
  max-width: 100%;
  text-align: center;
  background-color: ${props => props.bgColor ?  '#DA552F' : 'white'};
  color: ${props => props.bgColor ?  'white' : '#000000'};
  font-family: 'Noto Sans JP', sans-serif;
  font-size: 1.3rem;
  &:last-of-type{
    margin-right: 0;
  }
  &:hover{
    cursor: pointer;
    background-color: ${props => props.bgColor ?  '#bcbcbc' : '#DA552F'};
    color: ${props => props.bgColor ?  '#2b2b2b' : '#fcfcfc'};
    border: 1px solid ${props => props.bgColor ?  '#bcbcbc' : '#DA552F'};
  }
`;




export default Buton;
