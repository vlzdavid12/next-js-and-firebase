import React from "react";
import Header from "./Header";
import {Global, css} from '@emotion/react'
import Head from 'next/head';

const Layout = props => {
    return (
        <>
            <Global
                styles={css`
                  :root {
                    --gris: #3d3d3d;
                    --gris2: #6f6f6;
                    --gris3: #e1e1e1;
                    --naranja: #DA552F;
                  }

                  html {
                    font-size: 62.5%;
                    box-sizing: border-box
                  }

                  *, *:before, *:after {
                    box-sizing: inherit;
                  }

                  body {
                    font-size: 1.6rem;
                  }
                  
                  p, label, span{
                   font-family: 'Noto Sans JP', sans-serif;
                  }

                  h1, h2, h3 {
                    margin: 0 0 2rem 0;
                    line-height: 1.5;
                  }

                  h1, h2 {
                    font-family: 'Noto Sans JP', sans-serif;
                  },
                  
                h3, h4, h5 {
                  font-family: 'Roboto', sans-serif;
                },
                
                ul {
                  list-style: none;
                  margin: 0;
                  padding: 0
                }
                img{
                width: 100%;
                }
                `}

            />
            <Head>
                <html lang="es"/>
                <title>Products Hunt Firebase and Next.js</title>
                <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&family=Roboto:wght@300&display=swap"
                      rel="stylesheet"/>
                <link href="/static/css/app.css" rel="stylesheet"/>
            </Head>
            <Header/>
            <main>
                {props.children}
            </main>
        </>)
}
export default Layout
