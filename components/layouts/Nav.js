import React, {useContext} from "react";
import Link from "next/link";
import styled from "@emotion/styled";

//Start Context
import {FirebaseContext} from '../../firebase/';

const NavBar = styled.nav`
  padding-left: 2rem;

  a {
    font-size: 1.5rem;
    margin-left: 2rem;
    color: var(--gris2);
    font-family: "PT Sans", sans-serif;
    text-decoration: none;

    &:last-of-type {
      margin-right: 0;
    }
    
    &:hover{
     color:var(--naranja)
    }
  }`;


const Nav = () => {
    const {user} = useContext(FirebaseContext);
    return (
        <NavBar>
            <Link href="/">Inicio</Link>
            <Link href="/populares">Populares</Link>
            {user&&(<Link href="/nuevo-producto">Nuevos Productos</Link>)}

        </NavBar>
    )
}
export default Nav;
