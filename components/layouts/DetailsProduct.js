import React from 'react';
import styled from '@emotion/styled';
import {css} from '@emotion/react';
import formatDistanceToNow from 'date-fns/formatDistanceToNow'
import {es} from 'date-fns/locale'
import Link from 'next/link'


const Imagen = styled.img`
  width: 250px;
  border-radius: 4px;
`

const Description = styled.div`
   display: flex;
   padding-left: 10px;
   width: 60%;
`

const Title = styled.h2`
  margin: 0px;
  &:hover{
    cursor: pointer;
  }
`

const BTNComments = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  background: transparent;
  border: 1px solid  #dfdfdf;
  border-radius:5px;
  &:hover{
    cursor:pointer;
  }
`

const Votos = styled.div`
  flex: 0 0 0 auto;
  padding: 15px;
  border: 1px solid  #dfdfdf;
  height: min-content;
  p{
    text-align: center;
    margin: 0px;
  }
  
`

const DetailsProduct = ({result}) => {

    const {business, comments, nombre, url, votes, create, desc, id, urlimagen, creado} = result;
    
    return (
        <li css={css`
          display: flex;
          border-bottom: 1px solid #e7e7e7;
          margin-bottom: 10px;
        `}>
            <div>
                <Imagen src={urlimagen} alt={nombre}/>
            </div>
            <Description>
            <div>
                <Link href="/productos/[id]" as={`/productos/${id}`}>
                    <Title>{nombre}</Title>
                </Link>
                <p css={
                    css`
                    font-size: 10px;
                    margin: 0;  
                  `
                }>Publicado: {formatDistanceToNow(new Date(create), {locale: es})} por {creado?.name} de {business}</p>
                <p css={css` font-size: 14px`}>{ (desc.length > 65)? `${desc.substr(0, 65)} [...]` : desc} </p>
                <BTNComments><i className="icon-comments"></i> Comentarios: {comments.length}</BTNComments>

            </div>
            </Description>
            <Votos>
                <i className="icon-votes"></i>
                <p>Votos:</p>
                <p>{votes}</p>
            </Votos>
        </li>
    )
}
export default DetailsProduct;
