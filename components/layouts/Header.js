import React, {useContext} from 'react';
import Link from "next/link";
import styled from '@emotion/styled';
import {css} from '@emotion/react';

import Search from "../ui/Search";
import Nav from "./Nav";
import Buton from "../ui/Buttom";

//Start Context
import {FirebaseContext} from '../../firebase/';

const ContenedorHeader = styled.div`
  max-width: 1200px;
  width: 95%;
  margin: 0 auto;
  @media (min-width: 768px) {
    display: flex;
    justify-content: space-between;
  }
`;

const Logo = styled.p`
  color: var(--naranja);
  font-size: 3rem;
  line-height: 0;
  font-weight: 700;
  font-family: 'Noto Sans JP', sans-serif;
  margin-right: 2rem;
  top:-4px;
  padding: 0;
  position: relative;
  &:hover{
    cursor: pointer;
  }
`;


const Header = () => {
    const {user, firebase} = useContext(FirebaseContext);
    return (

        <header
            css={
                css`
                  border-bottom: 1px solid var(--gris3);
                  padding: 1rem 0;
                `
            }
        >
            <ContenedorHeader>
                <div css={css`
                    display: flex;
                    align-items: center;
                      `}>
                    <Link href="/">
                        <Logo> P </Logo>
                    </Link>
                    {/*Search Heer*/}
                    <Search/>
                    {/*Nav Heer*/}
                    <Nav/>
                </div>
                <div css={
                    css`
                        display: flex;
                        align-items: center;
                    `
                }>
                    {user ? (
                        <>
                            <p css={css`
                              margin-right: 2rem;
                              font-family: 'Noto Sans JP', sans-serif;
                              font-size:1.2rem
                            `}>Hola: {user.displayName} </p>
                            <Buton
                                bgColor={false}
                                onClick={()=>firebase.closeSession()}
                            > Cerra Session </Buton>
                        </>
                    ) : (
                        <>
                            <Link href="/login">
                                <Buton bgColor={true} css={css`margin-right: 5px`}>Login</Buton>
                            </Link>
                            <Link href="/crear-cuenta">
                                <Buton bgColor={false}> Crear Cuenta </Buton>
                            </Link>
                        </>
                    )
                    }
                </div>
            </ContenedorHeader>
        </header>

    )
}

export default Header;
